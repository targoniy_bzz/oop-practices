<?php

//Базовый Класс и Наследование

//Создайте класс Animal, который имеет защищенное свойство name и публичный метод speak() .
//Расширьте класс Animal с помощью класса Dog, который переопределяет метод speak() .


class Animals
{
    protected $name;

    function __construct($name)
    {
        $this->name = $name;
    }
    function speak()
    {
        echo "Я $this->name и издаю звук";
    }
}

class Dog extends Animals
{
    /**
     * @var mixed
     */


    function __construct($name)
    {
        $this->name = $name;
    }
    function speak()
    {
        echo "Я $this->name и я гавкаю";
    }
}

$barbos = new Dog("Барбос");
$barbos->speak();
