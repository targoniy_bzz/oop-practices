<?php

class Message
{
    private $text; //cвойство
    private $author;
    private $id;

    private static $counter = 0;

    function __construct($text, $author)
    {
        //только для статичных элементов
        self::$counter++;

        $this->id = self::$counter;

        $this->text = $text;

        $this->author = $author;
    }

    function displayInfo()
    {
        echo "Пользователь с id:".  $this->id . "по имени: $this->author \n" . "написал: $this->text";
    }
}

$messageM = [
    new Message('письки жопа', 'Андрей'), //1
    new Message('жопы письки', 'Данил'), //2
    new Message('Мишель....', 'Слава'), //3
];

foreach ($messageM as $message) {
    $message->displayInfo();
}
?>