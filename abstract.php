<?php
//Создайте абстрактный класс Shape
//с абстрактным методом getArea().
//Реализуйте дочерние классы Circle и Rectangle, переопределяющие метод getArea().

abstract class Shape
{
    abstract function getArea();
}

class Circle extends Shape {
    private $radius;

    /**
     * @param $radius
     */
    public function __construct($radius)
    {
        $this->radius = $radius;
    }


    function getArea()
    {
        echo M_PI*($this->radius**2)."\n";
    }
}

class Rectangle extends Shape
{
    private $width;
    private $height;

    /**
     * @param $width
     * @param $height
     */
    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }


    function getArea()
    {
       echo $this->height*$this->width ."\n";
    }
}

$circle = new Circle(5);
$circle->getArea();

$rectangle = new Rectangle(5, 4);
$rectangle->getArea();