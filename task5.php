<?php

//Инкапсуляция и Модификаторы Доступа
//
//Создайте класс BankAccount с приватными свойствами balance и accountNumber и методами для безопасного доступа к этим свойствами
//(например, getBalance() и setBalance()).

class BankAccount
{
    private $balance=400;
    private $accountNumber;

    function __construct($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    function getBalance()
    {
        return $this->balance;
    }

    function setBalance($balance)
    {
        $this->balance = $balance;
    }

    function balanceInfo()
    {
        echo "на аккаунте $this->accountNumber лежит $this->balance";
    }
}

$user1 = new BankAccount(123123);
$user1->balanceInfo();
