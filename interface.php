<!--Определите интерфейс Loggable с методом log().-->
<!--Создайте несколько классов, таких как FileLogger-->
<!--и DatabaseLogger, реализующих этот интерфейс.-->

<?php
interface Loggable
{
    function log($text);
}

class Filelogger implements Loggable
{

    function log($text)
    {
       echo "Я пишу текст $text в файл \n";
    }
}

class DatabaseLogger implements Loggable{

    function log($text)
    {
        echo "Я пишу текст $text в бд \n";
    }
}

$file = new Filelogger();
$file->log('кек');

$database = new DatabaseLogger();
$database->log('lol');
?>
