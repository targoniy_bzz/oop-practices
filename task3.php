<?php

//Разработайте класс DatabaseConnection
//с конструктором, который устанавливает соединение с базой данных,
//и деструктором, который закрывает соединение.
//Абстрактные Классы и Методы

class DatabaseConnection
{
    private string $db_name;
    function __construct($db_name)
    {
        $this->db_name = $db_name;

    }

    function connectionDB()
    {

            echo "подключение к базе $this->db_name активно";

    }


    function otherFunction($name)
    {
        echo "операция $name выполняется";
    }


    function __destruct()
    {
        echo "бд $this->db_name отключена";
    }



}

class otherBd
{

}

$baza = new DatabaseConnection('pisya');
$baza->connectionDB();

$baza->otherFunction('gbgbg');
$baza->otherFunction('12231321');
$baza->otherFunction('1231231');

$baza = new otherBd();

?>