<!--Создайте трейт Serializable, который предоставляет методы для-->
<!--сериализации и десериализации объектов. Используйте этот трейт в одном из ваших классов.-->
<?php

trait Serializable2
{
    public function serializate($data)
    {
         return json_encode($data);
    }
    public function deserializate($json)
    {
        return json_decode($json);
    }
}

class Ispolz
{
    use Serializable2;
}

$newdata = new Ispolz();
echo $newdata->serializate(array(1,2,3));

echo $newdata->deserializate('[1,2,3]');