<?php

//транспорт умеет бибикать
//наследник машина умеет кретить руль
//наследник самолет умеет крутить штурвал

class Transport
{

    public $name;


    /**
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function bibik()
    {
        echo "биби";
    }

    public function umenee()
    {
        echo "я не умею поворачивать руль\n";
    }

}

class Car extends Transport
{


    public function umenee()
    {
        echo "я умею поворачивать \n";
    }

}

class Plane extends Transport
{


    public function umenee()
    {
        echo "я умею поворачивать штурвал \n";
    }
}

$lada = new Transport('vaz 2114');
$lada->umenee();

$poezd = new Car('rzd');
$poezd->umenee();

$boing = new Plane('boing');
$boing->umenee();
?>