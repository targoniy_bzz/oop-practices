<?php


//Создайте класс Calculator
//с статическими методами для выполнения основных арифметическихопераций (например, add(), subtract()).

class Calculator
{

    public static function add($a, $b)
    {
        return $a + $b;
    }

    public static function subtract($a, $b)
    {
        return $a - $b;
    }

    public static function multiply($a, $b)
    {
        return $a * $b;
    }

    public static function divide($a, $b)
    {
        return $a / $b ;
    }
}

echo Calculator::add(2, 2);
echo Calculator::divide(2, 2);
echo Calculator::subtract(2, 2);
echo Calculator::multiply(2, 3);
